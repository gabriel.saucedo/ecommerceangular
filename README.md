# ecommerceAngular

Generalizando servicios y componentes para ecommerce en Angular 10
Usando este repositorio `https://github.com/zaracooper/lime-app` como referencia.
Proyecto completo en GitLab `https://gitlab.com/gabriel.saucedo/ecommerce`.

## Getting started

Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Add node_modules

```
npm install
```

## Estructura de la aplicacion
La aplicación se encuentra dividida en ciertas capas =>
### [data]
--- En esta capa se escuentran alojadas las interfaces y servicios de todos las pantallas que utilizaremos.
### [features]
-- En esta capa se encuentran creados las diferentes pantallas de la aplicacion por ahora solo se ha implementado el cart.
-- Cada pantalla se maneja a nivel de modulo ejemplo las pantallas relacionadas con cart tienen su propio modulo para estas en donde estan declaradas sus rutas de acceso.
### [Service]
--- Aqui se encuentran instanciados ciertos servicios para que se utilicen en la aplicación caso el local-storage así tambien como el httpService.
### [Shared]
--- En esta capa se encuentra instanciodos ciertos componentes que se pueden utilizar en cualquier componente de la app a travez de las vistas heredadas de angular @child.
