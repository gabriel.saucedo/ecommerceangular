import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { Address } from '../models/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private url: string = '';

  constructor(private http: HttpService, private eh: HttpErrorHandlerService) { }

  createAddress(address: Address): Observable<Address> {
    return this.http.post(this.url, address)
      .pipe(catchError(this.eh.handleError));
  }

  getAddress(id: string): Observable<Address> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.eh.handleError));
  }
}
