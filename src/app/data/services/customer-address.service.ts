import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { CustomerAddress } from '../models/customer-address';

@Injectable({
  providedIn: 'root'
})
export class CustomerAddressService {
  private url: string = ``;

  constructor(private http: HttpService, private errorHandler: HttpErrorHandlerService) { }

  createCustomerAddress(addressId: string, customerId: string): Observable<CustomerAddress> {
    return this.http.post(this.url, {
      addressId: addressId, customerId: customerId
    })
      .pipe(catchError(this.errorHandler.handleError));
  }

  getCustomerAddresses(): Observable<CustomerAddress[]> {
    return this.http.get(`${this.url}`)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getCustomerAddress(id: string): Observable<CustomerAddress> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }
}
