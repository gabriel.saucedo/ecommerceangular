import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { Customer } from '../models/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private url: string = ``;

  constructor(private http: HttpService, private errorHandler: HttpErrorHandlerService) { }

  createCustomer(email: string, password: string, firstName: string, lastName: string): Observable<Customer> {
    return this.http.post(this.url, {
      email: email,
      password: password,
      firstName: firstName,
      lastName: lastName
    })
      .pipe(catchError(this.errorHandler.handleError));
  }

  getCurrentCustomer(): Observable<Customer> {
    return this.http.get(`${this.url}/current`)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getCustomer(id: string): Observable<Customer> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }
}
