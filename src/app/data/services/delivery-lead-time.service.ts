import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { DeliveryLeadTime } from '../models/delivery-lead-time';

@Injectable({
  providedIn: 'root'
})
export class DeliveryLeadTimeService {
  private url: string = ``;

  constructor(private http: HttpService, private eh: HttpErrorHandlerService) { }

  getDeliveryLeadTimes(): Observable<DeliveryLeadTime[]> {
    return this.http.get(this.url)
      .pipe(catchError(this.eh.handleError));
  }
}
