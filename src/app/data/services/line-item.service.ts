import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { LineItem } from '../models/line-item';

@Injectable({
  providedIn: 'root'
})
export class LineItemService {
  private url: string = ``;

  constructor(private http: HttpService, private errorHandler: HttpErrorHandlerService) { }

  createLineItem(lineItem: LineItem): Observable<LineItem> {
    return this.http.post(this.url, lineItem)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getLineItem(id: string): Observable<LineItem> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }

  updateLineItem(id: string, quantity: number): Observable<LineItem> {
    return this.http.patch(`${this.url}/${id}`, { quantity: quantity })
      .pipe(catchError(this.errorHandler.handleError));
  }

  deleteLineItem(id: string): Observable<LineItem> {
    return this.http.delete(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }
}
