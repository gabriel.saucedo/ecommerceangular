import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { Order, GetOrderParams, UpdateOrderParams } from '../models/order';
import { Shipment } from '../models/shipment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private url: string = `https://tedregal-test.commercelayer.io/`;

  constructor(
    private http: HttpService,
    private errorHandler: HttpErrorHandlerService) { }

  createOrder(): Observable<Order> {
    return this.http.post(this.url, {})
      .pipe(catchError(this.errorHandler.handleError));
  }

  getOrder(id: string, orderParam: GetOrderParams): Observable<Order> {
    let params = {};
    if (orderParam != GetOrderParams.none) {
      params = { [orderParam]: 'true' };
    }

    return this.http.getWithParams(`${this.url}/${id}`, { params: params })
      .pipe(catchError(this.errorHandler.handleError));
  }

  updateOrder(order: Order, params: UpdateOrderParams[]): Observable<Order> {
    let updateParams = [];
    for (const param of params) {
      updateParams.push(param.toString());
    }

    return this.http.patchWithParams(
      `${this.url}/${order.id}`,
      order,
      { params: { 'field': updateParams } }
    )
      .pipe(catchError(this.errorHandler.handleError));
  }

  getOrderShipments(id: string): Observable<Shipment[]> {
    return this.http.get(`${this.url}/${id}/shipments`)
      .pipe(catchError(this.errorHandler.handleError));
  }
}
