import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';
import { environment } from 'src/environments/environment';
import { PaypalPayment } from '../models/paypal-payment';

@Injectable({
  providedIn: 'root'
})
export class PaypalPaymentService {
  private url: string = ``;

  constructor(private http: HttpService, private errorHandler: HttpErrorHandlerService) { }

  createPaypalPayment(payment: PaypalPayment): Observable<PaypalPayment> {
    return this.http.post(this.url, payment)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getPaypalPayment(id: string): Observable<PaypalPayment> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }

  updatePaypalPayment(id: string, paypalPayerId: string): Observable<PaypalPayment> {
    return this.http.patchWithParams(
      `${this.url}/${id}`,
      { paypalPayerId: paypalPayerId }
    )
      .pipe(catchError(this.errorHandler.handleError));
  }
}
