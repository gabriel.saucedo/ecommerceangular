import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Sku } from 'src/app/data/models/sku';
import { HttpErrorHandlerService } from 'src/app/service/http-error-handler.service';
import { HttpService } from 'src/app/service/http.service';

@Injectable({
  providedIn: 'root'
})
export class SkuService {
  private url: string = ``;

  constructor(private http: HttpService, private errorHandler: HttpErrorHandlerService) { }

  getSku(id: string): Observable<Sku> {
    return this.http.get(`${this.url}/${id}`)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getSkus(page: number, pageSize: number): Observable<Sku[]> {
    return this.http.getWithParams(
      this.url,
      {
        params: {
          'page': page.toString(),
          'pageSize': pageSize.toString()
        }
      })
      .pipe(catchError(this.errorHandler.handleError));
  }
}
