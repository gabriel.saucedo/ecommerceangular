import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandlerService {

  constructor() { }

  handleError(errorResponse: HttpErrorResponse): Observable<any> {
    let displayMessage = '';

    if (errorResponse.error instanceof ErrorEvent) {
      displayMessage = `Client-side error: ${errorResponse.error.message}`;
    } else {
      displayMessage = `Server-side error: ${errorResponse.message}`;
    }

    return throwError(displayMessage);
  }
}
