import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  post(url: string, data: any): Observable<any> {
    return this.http.post(url, data);
  }

  get(url: string): Observable<any> {
    return this.http.get(url);
  }
  getWithParams(url: string, params: any): Observable<any> {
    return this.http.get(url, { params });
  }

  patch(url: string, data: any) {
    return this.http.patch(url, data);
  }

  patchWithParams(url: string, data?: any, params?: any) {
    return this.http.patch(url, data, { params });
  }

  delete(url: string) {
    return this.http.delete(url);
  }
}
