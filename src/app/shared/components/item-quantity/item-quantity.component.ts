import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-item-quantity',
  templateUrl: './item-quantity.component.html',
  styleUrls: ['./item-quantity.component.scss']
})
export class ItemQuantityComponent implements OnInit {

  @Input() quantity: number = 0;
  @Input() maxValue?: number = 0;
  @Input() disabled?: boolean = false;
  @Output() setQuantityEvent = new EventEmitter<number>();
  values: number[] = [];

  constructor() { }

  ngOnInit(): void {
    if (this.maxValue) {
      for (let index = 1; index < this.maxValue; index++) {
        this.values.push(index);
      }
    }
  }

  setQuantity(value: number) {
    this.setQuantityEvent.emit(value);
  }

}
